namespace WPFLista.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class index : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Students", "IndexNo", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Students", "IndexNo", c => c.Int(nullable: false));
        }
    }
}
