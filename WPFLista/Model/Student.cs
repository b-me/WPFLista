﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFLista.Model
{
    class Student
    {
        public int StudentId { get; set; }
        [Required(ErrorMessage = "Nie podano numeru indeksu")]
        public string IndexNo { get; set; }
        [MaxLength(32, ErrorMessage = "Imię nie powinno być dłuższe niż 32 znaki"), Required(ErrorMessage = "Nie podano imienia")]
        public string FirstName { get; set; }
        [MaxLength(32, ErrorMessage = "Nazwisko nie powinno być dłuższe niż 32 znaki"), Required(ErrorMessage = "Nie podano nazwiska")]
        public string LastName { get; set; }
        [Required(ErrorMessage = "Pole")]
        public int GroupId { get; set; }
        public virtual Group Group { get; set; }
    }
}
