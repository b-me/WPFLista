﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFLista.Model
{
    class StorageException : Exception {
        public StorageException() : base() { }
        public StorageException(string message) : base(message) { }
        public StorageException(string message, System.Exception inner) : base(message, inner) { }

        protected StorageException(System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) { }
    }

    class Storage
    {
        private void HandleValidationError(DbEntityValidationException dbEx)
        {
            var errorMessage = new StringBuilder();
            foreach (DbEntityValidationResult entityErr in dbEx.EntityValidationErrors)
            {
                foreach (DbValidationError error in entityErr.ValidationErrors)
                {
                    errorMessage.AppendLine(error.ErrorMessage);
                }
            }
            throw new StorageException(errorMessage.ToString());
        }

        public List<Student> getStudents(Group group)
        {
            using (var db = new StorageContext())
            {
                if (group != null)
                    return db.Students.Where(s => s.GroupId == group.GroupId).ToList();
                return new List<Student>();
            }
        }

        public List<Group> getGroups()
        {
            using (var db = new StorageContext())
            {
                return db.Groups.ToList();
            }
        }

        public void createStudent ( string firstName , string lastName , string indexNo , int groupId ) {
            using (var db = new StorageContext ()) {
                var student = new Student { FirstName = firstName , LastName = lastName ,
                                            IndexNo = indexNo , GroupId = groupId };
                db.Students.Add(student);
                try
                {
                    db.SaveChanges();
                }
                catch (DbEntityValidationException dbEx)
                {
                    HandleValidationError(dbEx);
                }
                catch (DbUpdateException dbEx)
                {
                    throw new StorageException("Wystąpił błąd bazy", dbEx);
                }
            }
        }

        public void updateStudent (Student st) {
            using (var db = new StorageContext ()) {
                var original = db.Students.Find(st.StudentId);
                if (original != null) {
                    original.FirstName = st.FirstName;
                    original.LastName = st.LastName;
                    original.IndexNo = st.IndexNo;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbEntityValidationException dbEx)
                    {
                        HandleValidationError(dbEx);
                    }
                    catch (DbUpdateException dbEx)
                    {
                        throw new StorageException("Wystąpił błąd bazy", dbEx);
                    }
                }
            }
        }

        public void deleteStudent (Student st) {
            using ( var db = new StorageContext ()) {
                var original = db . Students . Find ( st . StudentId );
                if ( original != null ) {
                    db.Students.Remove(original);
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (DbUpdateException dbEx)
                    {
                        throw new StorageException("Wystąpił błąd bazy", dbEx);
                    }
                }
            }
        }
    }
}
