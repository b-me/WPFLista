﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using WPFLista.Model;

namespace WPFLista.ViewModel
{
    class MainWindowViewModel : ViewModelBase
    {
        private Storage _storage;
        private Group _selectedGroup;
        private Student _selectedStudent;
        private List<Student> _students;
        private string _studentFirstName;
        private string _studentLastName;
        private string _studentIndexNo;

        public ICommand ButtonCommand { get; set; }


        public List<Group> Groups { get; set; }

        public List<Student> Students
        {
            get
            {
                return _students;
            }
            set
            {
                if (value != _students)
                    _students = value;
                base.OnPropertyChanged("Students");
            }
        }

        public string StudentFirstName 
        { 
            get 
            {
                return _studentFirstName;
            }
            set 
            {
                if (value != _studentFirstName)
                    _studentFirstName = value;
                base.OnPropertyChanged("StudentFirstName");
            }
        }

        public string StudentLastName
        {
            get
            {
                return _studentLastName;
            }
            set
            {
                if (value != _studentLastName)
                    _studentLastName = value;
                base.OnPropertyChanged("StudentLastName");
            }
        }

        public string StudentIndexNo
        { 
            get 
            {
                return _studentIndexNo;
            }
            set 
            {
                if (value != _studentIndexNo)
                    _studentIndexNo = value;
                base.OnPropertyChanged("StudentIndexNo");
            }
        }

        public Student SelectedStudent
        {
            get
            {
                return _selectedStudent;
            }
            set
            {
                if (value != _selectedStudent)
                    _selectedStudent = value;
                StudentFirstName = value != null ? value.FirstName : "";
                StudentLastName = value !=  null ? value.LastName : "";
                StudentIndexNo = value != null ? value.IndexNo : "";
                if (ButtonCommand != null)
                    (ButtonCommand as RelayCommand).OnCanExecuteChanged();
                base.OnPropertyChanged("SelectedStudent");
            }
        }

        public Group SelectedGroup
        {
            get
            {
                return _selectedGroup;
            }
            set
            {
                if (value != _selectedGroup)
                    _selectedGroup = value;
                if (ButtonCommand != null)
                    (ButtonCommand as RelayCommand).OnCanExecuteChanged();
                base.OnPropertyChanged("SelectedGroup");
            }
        }

        private void LoadStudents()
        {
            Students = _storage.getStudents(SelectedGroup);
            SelectedStudent = null;
        }

        void MainWindowViewModel_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName.Equals("SelectedGroup"))
                LoadStudents();
        }

        public MainWindowViewModel()
        {
            _storage = new Storage();
            Groups = _storage.getGroups();

            this.PropertyChanged += MainWindowViewModel_PropertyChanged;

            SelectedGroup = Groups.Count > 0 ? Groups.First() : null;
            StudentFirstName = "";
            StudentLastName = "";
            StudentLastName = "";
            ButtonCommand = new RelayCommand(
                new Action<object>(delegate(object obj)
                {
                    try
                    {
                        var action = obj as string;
                        switch (action)
                        {
                            case "New":
                                _storage.createStudent(
                                    StudentFirstName, StudentLastName,
                                    StudentIndexNo,
                                    SelectedGroup.GroupId);
                                break;
                            case "Update":
                                SelectedStudent.FirstName = StudentFirstName;
                                SelectedStudent.LastName = StudentLastName;
                                SelectedStudent.IndexNo = StudentIndexNo;
                                _storage.updateStudent(SelectedStudent);
                                break;
                            case "Delete":
                                _storage.deleteStudent(SelectedStudent);
                                break;
                        }
                        LoadStudents();
                    }
                    catch (StorageException ex)
                    {
                        System.Windows.MessageBox.Show(ex.Message);
                    }
                }),
                new Func<object, bool>(delegate(object obj)
                {
                    var action = obj as string;
                    if ((action.Equals("Delete") || action.Equals("Update")) && SelectedStudent == null)
                        return false;
                    if (action.Equals("New") && SelectedGroup == null)
                        return false;
                    return true;
                }
                )
            );
        }

    }
}
